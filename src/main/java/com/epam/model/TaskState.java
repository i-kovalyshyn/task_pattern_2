package com.epam.model;

import lombok.extern.log4j.Log4j2;

public interface TaskState {
    @Log4j2
    final class LogHolder {
    }

    default void addTask(Task task) {
        LogHolder.log.info("Add task is not allowed");
    }

    default void toInProgress(Task task) {
        LogHolder.log.info("Add task to progress is not allowed");
    }

    default void codeReview(Task task) {
        LogHolder.log.info("Add task to code review is not allowed");
    }

    default void toDone(Task task) {
        LogHolder.log.info("Add task to done is not allowed");
    }
}
