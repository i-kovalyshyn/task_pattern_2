package com.epam.controller;

public interface BoardController {
    void addTask();

    void addTaskToInProgress();

    void addTaskToCodeReview();

    void addTaskToDone();

    void printTasks();
}
