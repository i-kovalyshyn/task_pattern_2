package com.epam.view;

import com.epam.controller.BoardController;
import lombok.extern.log4j.Log4j2;

import java.util.Scanner;

@Log4j2
public class Menu {
    private BoardController boardController;

    public Menu(BoardController boardController) {
        this.boardController = boardController;

        Scanner scanner = new Scanner(System.in);
        String keyMenu;
        do {
            log.info("1 - Add Task");
            log.info("2 - To In Progress");
            log.info("3 - To Code Review");
            log.info("4 - To Done");
            log.info("5 - Print tasks");
            log.info("Q - Exit");
            log.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        boardController.addTask();
                        break;
                    case "2":
                        boardController.addTaskToInProgress();
                        break;
                    case "3":
                        boardController.addTaskToCodeReview();
                        break;
                    case "4":
                        boardController.addTaskToDone();
                        break;
                    case "5":
                        boardController.printTasks();
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
